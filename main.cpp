#include <iostream>
#include <QDirIterator>
#include <QFileInfo>
#include <QDateTime>

int main(int argc, char * argv[]) {
    if (argc < 3) {
	std::cout << "Insufficient parameters given" << std::endl;
	return 1;
    }

    bool valid_format;
    int ammount = QString(argv[2]).toInt(&valid_format);
    int counter = 0;

    if (!valid_format || ammount < 0)
        std::cout << "Invalid number given" << std::endl;

    QDirIterator it(argv[1], QDirIterator::Subdirectories | QDirIterator::FollowSymlinks);
    QString filePath("");

    while (it.hasNext() && counter < ammount) {
        QFileInfo fileInfo = it.fileInfo();
	    if (fileInfo.path() != filePath) {
            std::cout << "Directory: " << fileInfo.path().toStdString() << std::endl;
            filePath = fileInfo.path();
            counter++;
        }

        if (fileInfo.fileName() != QStringLiteral(".") && fileInfo.fileName() != QStringLiteral("..")) {
            std::cout << fileInfo.size()/1024 <<" kb " << fileInfo.created().toString("yyyy-MM-dd hh:mm").toStdString() << " " << fileInfo.fileName().toStdString()  << std::endl;
            counter++;
        }
        it.next();
    }

    return 0;
}